package com.company.productflowaxon.controller;

import com.company.productflowaxon.command.CompleteOrderCommand;
import com.company.productflowaxon.command.CreateOrderCommand;
import com.company.productflowaxon.command.CreateProductCommand;
import com.company.productflowaxon.command.ProcessOrderCommand;
import java.util.concurrent.Future;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/products")
public class CommandController {

    private final CommandGateway commandGateway;

    @PostMapping("/order")
    public Future<String> createOrder(@RequestBody CreateOrderCommand command) {
        return commandGateway.send(command);
    }

    @PostMapping
    public Future<String> createProduct(@RequestBody CreateProductCommand command) {
        return commandGateway.send(command);
    }

    @PostMapping("/order/process")
    public Future<Void> approveProduct(@RequestBody ProcessOrderCommand command) {
        return commandGateway.send(command);
    }

    @PostMapping("/order/complete")
    public Future<Void> approveProduct(@RequestBody CompleteOrderCommand command) {
        return commandGateway.send(command);
    }

}
