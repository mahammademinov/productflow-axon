package com.company.productflowaxon.controller;

import com.company.productflowaxon.model.dto.OrderView;
import com.company.productflowaxon.model.dto.ProductView;
import com.company.productflowaxon.query.FindOrderQuery;
import com.company.productflowaxon.query.FindProductQuery;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/products")
@RequiredArgsConstructor
public class QueryController {

    private final QueryGateway queryGateway;

    @GetMapping("/order/{orderId}")
    public CompletableFuture<OrderView> handleOrder(@PathVariable("orderId") String orderId) {
        return queryGateway.query(new FindOrderQuery(orderId),
                ResponseTypes.instanceOf(OrderView.class));
    }

    @GetMapping("{productId}")
    public CompletableFuture<ProductView> handleProduct(@PathVariable("productId") String productId) {
        return queryGateway.query(new FindProductQuery(productId),
                ResponseTypes.instanceOf(ProductView.class));
    }

}
