package com.company.productflowaxon.dao.repository;

import com.company.productflowaxon.dao.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {
}
