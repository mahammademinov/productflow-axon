package com.company.productflowaxon.dao.repository;

import com.company.productflowaxon.dao.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, String> {
}
