package com.company.productflowaxon.dao.entity;

import com.company.productflowaxon.model.enums.OrderStatus;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class Order {

    @Id
    private String orderId;
    private String customerId;
    private String productId;
    private Integer quantity;

    @Enumerated(value = EnumType.STRING)
    private OrderStatus status;

}
