package com.company.productflowaxon.model.dto;

import lombok.Data;

@Data
public class ProductView {

    private String productId;
    private String name;
    private Integer quantity;

}
