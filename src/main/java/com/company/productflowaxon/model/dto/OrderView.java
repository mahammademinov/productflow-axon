package com.company.productflowaxon.model.dto;

import lombok.Data;

@Data
public class OrderView {

    private String orderId;
    private String customerId;
    private String productId;
    private Integer quantity;

}
