package com.company.productflowaxon.model.dto;

import com.company.productflowaxon.model.enums.OrderStatus;
import lombok.Data;

@Data
public class OrderDto {

    private String orderId;
    private String customerId;
    private String productId;
    private Integer quantity;
    private OrderStatus orderStatus;
}
