package com.company.productflowaxon.model.enums;

import com.company.productflowaxon.statemachine.order.transitions.Approve;
import com.company.productflowaxon.statemachine.order.transitions.Reject;
import com.company.productflowaxon.statemachine.order.transitions.Submit;
import java.util.Arrays;
import java.util.List;

public enum OrderStatus {

    CREATED(Submit.NAME, Reject.NAME),
    PROCESSING(Approve.NAME),
    CANCELLED,
    COMPLETED;

    private final List<String> transitions;

    OrderStatus(String... transitions) {
        this.transitions = Arrays.asList(transitions);
    }

    public List<String> getTransitions() {
        return transitions;
    }
}
