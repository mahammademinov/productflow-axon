package com.company.productflowaxon.event;

import lombok.Value;

@Value
public class OrderProcessingEvent {

    String orderId;
    String customerId;

    String productId;
}
