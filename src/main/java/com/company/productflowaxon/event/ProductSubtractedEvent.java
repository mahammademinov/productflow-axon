package com.company.productflowaxon.event;

import lombok.Value;

@Value
public class ProductSubtractedEvent {

    String productId;
    Integer quantity;
}

