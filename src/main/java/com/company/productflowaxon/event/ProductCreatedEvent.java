package com.company.productflowaxon.event;

import lombok.Value;

@Value
public class ProductCreatedEvent {

    String productId;
    String name;
    Integer quantity;
}
