package com.company.productflowaxon.event;

import com.company.productflowaxon.model.dto.OrderDto;
import com.company.productflowaxon.model.enums.OrderStatus;
import lombok.Value;

@Value
public class OrderTransitionedEvent {

    Object source;
    OrderDto orderDto;
    OrderStatus orderStatus;

}
