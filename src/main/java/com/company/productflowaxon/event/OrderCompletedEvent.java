package com.company.productflowaxon.event;

import lombok.Value;

@Value
public class OrderCompletedEvent {

    String orderId;
    String customerId;
    String productId;
    Integer quantity;

}
