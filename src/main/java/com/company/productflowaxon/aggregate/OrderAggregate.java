package com.company.productflowaxon.aggregate;

import com.company.productflowaxon.command.CompleteOrderCommand;
import com.company.productflowaxon.command.CreateOrderCommand;
import com.company.productflowaxon.command.ProcessOrderCommand;
import com.company.productflowaxon.event.OrderCompletedEvent;
import com.company.productflowaxon.event.OrderCreatedEvent;
import com.company.productflowaxon.event.OrderProcessingEvent;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.ElementCollection;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Slf4j
@Data
@Aggregate
@NoArgsConstructor
public class OrderAggregate {

    @AggregateIdentifier
    private String orderId;

    @ElementCollection
    private final Map<String, Integer> products = new HashMap<>();

    @CommandHandler
    public OrderAggregate(CreateOrderCommand command) {
        log.info("[Aggregate][Command] Handle command: {}", command);
        AggregateLifecycle.apply(
                new OrderCreatedEvent(command.getOrderId(), command.getCustomerId(), command.getProductId(),
                        command.getQuantity()));
    }

    @CommandHandler
    public void handle(ProcessOrderCommand command) {
        log.info("[Aggregate][Command] Handle command: {}", command);
        AggregateLifecycle.apply(new OrderProcessingEvent(orderId, command.getCustomerId(), command.getProductId()));
    }

    @CommandHandler
    public void handle(CompleteOrderCommand command) {
        log.info("[Aggregate][Command] Handle command: {}", command);
        AggregateLifecycle.apply(new OrderCompletedEvent(orderId, command.getCustomerId(), command.getProductId(),
                products.get(command.getProductId())));
    }

    @EventSourcingHandler
    public void on(OrderCreatedEvent event) {
        log.info("[Aggregate][Event] On event: {}", event);
        this.orderId = event.getOrderId();
        this.products.put(event.getProductId(), event.getQuantity());
    }

}
