package com.company.productflowaxon.aggregate;

import com.company.productflowaxon.command.CreateProductCommand;
import com.company.productflowaxon.command.SubtractProductCommand;
import com.company.productflowaxon.event.ProductCreatedEvent;
import com.company.productflowaxon.event.ProductSubtractedEvent;
import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Slf4j
@Data
@Aggregate
@NoArgsConstructor
public class ProductAggregate {

    @AggregateIdentifier
    private String productId;
    private String name;

    private Map<String, Integer> products;

    @CommandHandler
    public ProductAggregate(CreateProductCommand command) {
        log.info("[Aggregate][Command] Handle command: {}", command);
        this.productId = command.getProductId();
        this.products = new HashMap<>();
        AggregateLifecycle.apply(new ProductCreatedEvent(productId, command.getName(), command.getQuantity()));
    }

    @CommandHandler
    public void handle(SubtractProductCommand command) {
        log.info("[Aggregate][Command] Handle command: {}", command);
        AggregateLifecycle.apply(new ProductSubtractedEvent(productId, command.getQuantity()));
    }

    @EventSourcingHandler
    public void on(ProductSubtractedEvent event) {
        log.info("[Aggregate][Event] On event: {}", event);
        if (!products.containsKey(event.getProductId())) {
            throw new RuntimeException("Product not found: " + event.getProductId());
        }
        Integer value = products.get(event.getProductId());
        Integer newValue = Math.max(value - event.getQuantity(), 0);
        products.replace(event.getProductId(), newValue);
    }

    @EventSourcingHandler
    public void on(ProductCreatedEvent event) {
        log.info("[Aggregate][Event] On event: {}", event);
        if (!products.containsKey(event.getProductId())) {
            products.put(event.getProductId(), event.getQuantity());
        }
        this.productId = event.getProductId();
        this.name = event.getName();
    }

}
