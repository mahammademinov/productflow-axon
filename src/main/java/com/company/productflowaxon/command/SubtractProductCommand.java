package com.company.productflowaxon.command;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubtractProductCommand {

    @TargetAggregateIdentifier
    private String productId;

    @NotNull
    private Integer quantity;
}
