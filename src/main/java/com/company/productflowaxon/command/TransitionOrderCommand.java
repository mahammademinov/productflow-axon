package com.company.productflowaxon.command;

import com.company.productflowaxon.model.dto.OrderDto;
import com.company.productflowaxon.model.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransitionOrderCommand {

    Object source;
    OrderStatus orderStatus;
    OrderDto orderDto;
}
