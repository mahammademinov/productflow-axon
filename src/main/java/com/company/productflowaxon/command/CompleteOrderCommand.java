package com.company.productflowaxon.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompleteOrderCommand {

    @TargetAggregateIdentifier
    private String orderId;
    private String customerId;
    private String productId;
}
