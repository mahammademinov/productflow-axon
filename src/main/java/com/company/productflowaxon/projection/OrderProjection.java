package com.company.productflowaxon.projection;

import com.company.productflowaxon.dao.entity.Order;
import com.company.productflowaxon.dao.entity.Product;
import com.company.productflowaxon.dao.repository.OrderRepository;
import com.company.productflowaxon.dao.repository.ProductRepository;
import com.company.productflowaxon.event.OrderCompletedEvent;
import com.company.productflowaxon.event.OrderCreatedEvent;
import com.company.productflowaxon.event.OrderProcessingEvent;
import com.company.productflowaxon.event.OrderTransitionedEvent;
import com.company.productflowaxon.event.ProductSubtractedEvent;
import com.company.productflowaxon.model.dto.OrderDto;
import com.company.productflowaxon.model.dto.OrderView;
import com.company.productflowaxon.model.enums.OrderStatus;
import com.company.productflowaxon.query.FindOrderQuery;
import com.company.productflowaxon.statemachine.order.TransitionService;
import com.company.productflowaxon.statemachine.order.transitions.Approve;
import com.company.productflowaxon.statemachine.order.transitions.Submit;
import java.text.MessageFormat;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrderProjection {

    private final ModelMapper mapper;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final TransitionService<OrderDto> orderTransitionService;

    @EventHandler
    public void on(OrderCreatedEvent event) {
        log.info("[Projection][Order][Event] On event: {}", event);
        orderRepository.save(
                new Order(event.getOrderId(), event.getCustomerId(), event.getProductId(), event.getQuantity(),
                        OrderStatus.CREATED));
    }

    @EventHandler
    public void on(ProductSubtractedEvent event) {
        log.info("[Projection][Order][Event] On event: {}", event);
        Product product = productRepository.findById(event.getProductId())
                .orElseThrow(() -> new RuntimeException("Product does not exist"));
        if (product.getQuantity() - event.getQuantity() < 0) {
            throw new RuntimeException(MessageFormat.format("{} products are out of stock", event.getQuantity()));
        }
        product.setQuantity(product.getQuantity() - event.getQuantity());
        productRepository.save(product);
    }

    @EventHandler
    public void on(OrderProcessingEvent event) {
        log.info("[Projection][Order][Event] On event: {}", event);
        orderTransitionService.transition(event.getOrderId(), Submit.NAME);
    }

    @EventHandler
    public void on(OrderCompletedEvent event) {
        log.info("[Projection][Order][Event] On event: {}", event);
        orderTransitionService.transition(event.getOrderId(), Approve.NAME);
    }

    @EventHandler
    public void on(OrderTransitionedEvent event) {
        log.info("[Projection][Order][Event] On event: {}", event);
        log.info("Received order status changed event for order {} from state {} to state {} from source {}",
                event.getOrderDto().getOrderId(), event.getOrderStatus(),
                event.getOrderDto().getOrderStatus(), event.getSource());
    }

    @QueryHandler
    @Transactional
    public OrderView handle(FindOrderQuery query) {
        log.info("Querying order with id {}", query.getOrderId());
        Order foodCart = orderRepository.findById(query.getOrderId())
                .orElseThrow(() -> new RuntimeException("Order does not exist"));
        return mapper.map(foodCart, OrderView.class);
    }

}
