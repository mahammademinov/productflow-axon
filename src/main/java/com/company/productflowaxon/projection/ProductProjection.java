package com.company.productflowaxon.projection;

import com.company.productflowaxon.dao.entity.Product;
import com.company.productflowaxon.dao.repository.OrderRepository;
import com.company.productflowaxon.dao.repository.ProductRepository;
import com.company.productflowaxon.event.ProductCreatedEvent;
import com.company.productflowaxon.event.ProductSubtractedEvent;
import com.company.productflowaxon.model.dto.ProductView;
import com.company.productflowaxon.query.FindProductQuery;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
@Slf4j
public class ProductProjection {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final ModelMapper mapper;

    @EventHandler
    public void on(ProductCreatedEvent event) {
        log.info("[Projection][Product][Event] On event: {}", event);
        productRepository.save(
                new Product(event.getProductId(), event.getName(), event.getQuantity()));
    }

    @EventHandler
    public void on(ProductSubtractedEvent event) {
        log.info("[Projection][Product][Event] On event: {}", event);
        Product product = productRepository.findById(event.getProductId())
                .orElseThrow(() -> new RuntimeException("Product does not exist"));
        product.setQuantity(Math.max(product.getQuantity() - event.getQuantity(), 0));
    }

    @QueryHandler
    @Transactional
    public ProductView handle(FindProductQuery query) {
        log.info("Querying product with id {}", query.getProductId());
        Product product = productRepository.findById(query.getProductId())
                .orElseThrow(() -> new RuntimeException("Order does not exist"));
        return mapper.map(product, ProductView.class);
    }

}
