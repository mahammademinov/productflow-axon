package com.company.productflowaxon;

import org.axonframework.springboot.autoconfig.JdbcAutoConfiguration;
import org.axonframework.springboot.autoconfig.JpaEventStoreAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class,
        JpaEventStoreAutoConfiguration.class, JdbcAutoConfiguration.class})
@EntityScan(basePackages = {"com.company.productflowaxon.*", "org.axonframework.modelling.saga.repository.jpa",
        "org.axonframework.eventsourcing.eventstore.jpa", "org.axonframework.eventhandling.tokenstore.jpa"})
@ComponentScan(basePackages = {"com.company.productflowaxon.*"})
@EnableAspectJAutoProxy
public class ProductFlowAxonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductFlowAxonApplication.class, args);
    }

}
