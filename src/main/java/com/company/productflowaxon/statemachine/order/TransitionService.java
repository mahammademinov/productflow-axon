package com.company.productflowaxon.statemachine.order;

import java.util.List;

public interface TransitionService<T> {

    T transition(String id, String transition);

    List<String> getAllowedTransitions(String id);
}
