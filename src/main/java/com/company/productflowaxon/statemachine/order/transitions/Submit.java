package com.company.productflowaxon.statemachine.order.transitions;

import com.company.productflowaxon.event.ProductSubtractedEvent;
import com.company.productflowaxon.model.dto.OrderDto;
import com.company.productflowaxon.model.enums.OrderStatus;
import com.company.productflowaxon.projection.ProductProjection;
import com.company.productflowaxon.statemachine.order.Transition;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class Submit implements Transition<OrderDto> {

    public static final String NAME = "submit";

    private final ProductProjection productProjection;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getTargetStatus() {
        return OrderStatus.PROCESSING;
    }

    @Override
    public void applyProcessing(OrderDto orderDto) {
        log.info("Order is transitioning to in review state {}", orderDto.getOrderId());
        productProjection.on(new ProductSubtractedEvent(orderDto.getProductId(), orderDto.getQuantity()));
    }
}
