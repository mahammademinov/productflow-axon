package com.company.productflowaxon.statemachine.order.transitions;

import com.company.productflowaxon.model.dto.OrderDto;
import com.company.productflowaxon.model.enums.OrderStatus;
import com.company.productflowaxon.statemachine.order.Transition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Reject implements Transition<OrderDto> {

    public static final String NAME = "reject";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getTargetStatus() {
        return OrderStatus.CANCELLED;
    }

    @Override
    public void applyProcessing(OrderDto orderDto) {
        log.info("Order is transitioning to rejected state {}", orderDto.getOrderId());
    }
}
