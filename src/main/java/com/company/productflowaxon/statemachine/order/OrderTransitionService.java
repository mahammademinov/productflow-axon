package com.company.productflowaxon.statemachine.order;

import com.company.productflowaxon.dao.entity.Order;
import com.company.productflowaxon.dao.repository.OrderRepository;
import com.company.productflowaxon.model.dto.OrderDto;
import com.company.productflowaxon.model.enums.OrderStatus;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class OrderTransitionService implements TransitionService<OrderDto> {

    private Map<String, Transition<OrderDto>> transitionsMap;
    private final List<Transition<OrderDto>> transitions;
    private final ModelMapper mapper;
    private final OrderRepository orderRepository;

    private final CommandGateway commandGateway;

    public List<String> getAllowedTransitions(final String id) {
        final Order order = orderRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Unknown order: " + id));
        return order.getStatus().getTransitions();
    }

    @Transactional
    public OrderDto transition(final String id, final String transition) {
        final Transition<OrderDto> orderTransition = transitionsMap.get(transition.toLowerCase());
        if (orderTransition == null) {
            throw new IllegalArgumentException("Unknown transition: " + transition);
        }
        return orderRepository.findById(id)
                .map(order -> {
                    checkAllowed(orderTransition, order.getStatus());
                    orderTransition.applyProcessing(mapper.map(order, OrderDto.class));
                    return updateStatus(order, orderTransition.getTargetStatus());
                })
                .map(u -> mapper.map(u, OrderDto.class))
                .orElseThrow(() -> new IllegalArgumentException("Unknown order: " + id));
    }

    @PostConstruct
    private void initTransitions() {
        final Map<String, Transition<OrderDto>> transitionHashMap = new HashMap<>();
        for (final Transition<OrderDto> orderTransition : transitions) {
            if (transitionHashMap.containsKey(orderTransition.getName())) {
                throw new IllegalStateException("Duplicate transition :" + orderTransition.getName());
            }
            transitionHashMap.put(orderTransition.getName(), orderTransition);
        }
        this.transitionsMap = transitionHashMap;
    }

    private Object updateStatus(final Order order, final OrderStatus targetStatus) {
        order.setStatus(targetStatus);
        return orderRepository.save(order);
    }

    private void checkAllowed(Transition<OrderDto> orderTransition, OrderStatus status) {
        Set<OrderStatus> allowedSourceStatuses = Stream.of(OrderStatus.values())
                .filter(s -> s.getTransitions().contains(orderTransition.getName()))
                .collect(Collectors.toSet());
        if (!allowedSourceStatuses.contains(status)) {
            throw new RuntimeException(
                    "The transition from the current state " + status.name() + "  to the target state "
                            + orderTransition.getTargetStatus().name() + "  is not allowed!");
        }
    }

}
