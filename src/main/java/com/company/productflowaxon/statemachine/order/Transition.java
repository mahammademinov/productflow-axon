package com.company.productflowaxon.statemachine.order;

import com.company.productflowaxon.model.enums.OrderStatus;

public interface Transition<T> {

    String getName();

    OrderStatus getTargetStatus();

    void applyProcessing(T order);
}
